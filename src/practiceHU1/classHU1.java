package practiceHU1;

public class classHU1 {
	public static String toxicidad(double toxicidad) {
		
		String response = "ERROR"; 
		
		if(toxicidad < 0)
			response = "ERROR";
		
		if(toxicidad >= 0 && toxicidad <= 3.8)
			response = "VERDE";
		
		if(toxicidad >= 3.81 && toxicidad <= 8.25)
			response = "AZUL";
		
		if(toxicidad >= 8.26 && toxicidad <= 15)
			response = "AMARILLO";
		
		if(toxicidad > 15)
			response = "ROJO";
		
		return response;
	}
	
	public static String getLevelToxicidad(int PH, String NivelPlasmatico, String Concentracion, String Acidez, String Tipo, String Composicion) {
		
		double result = (PH + (NivelPlasmatico == "10-20" ? 8 : 6)) + ((Concentracion == "ALTA" ? 20 : Concentracion == "BAJA" ? 10 : 15) * (Acidez == "BASICO" ? 2 : 4));
		result = result / ((Tipo == "MTP" ? 5 : 3) + (Composicion == "ACTIVO" ? 2 : 4));
		
		return toxicidad(result);
	}
}
