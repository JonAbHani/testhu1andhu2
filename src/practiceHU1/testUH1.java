package practiceHU1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testUH1 {

	@Test
	void testError() {
		assertEquals(classHU1.toxicidad(-2), "ERROR");
	}

	@Test 
	void testVerde() {
		assertEquals(classHU1.toxicidad(3.8), "VERDE");
	}
	
	@Test 
	void testAzul() {
		assertEquals(classHU1.toxicidad(8.25), "AZUL");
	}
	
	@Test
	void testAmarillo() {
		assertEquals(classHU1.toxicidad(15), "AMARILLO");
	}
	
	@Test
	void testRojo() {
		assertEquals(classHU1.toxicidad(15.1), "ROJO");
	}
	
	@Test
	void testGradoToxicidad(){											   
		assertEquals(classHU1.getLevelToxicidad(7, "10-20", "ALTA", "BASICO", "MTP", "ACTIVO"), "AZUL"); //7.85
		assertEquals(classHU1.getLevelToxicidad(7, "20-30", "BAJA", "ALCALINO", "HOMEOPATICO", "EXCIPIENTE"), "AZUL"); //7.57
		assertEquals(classHU1.getLevelToxicidad(7, "10-20", "MEDIA", "BASICO", "MTP", "ACTIVO"), "AZUL"); //6.42
		assertEquals(classHU1.getLevelToxicidad(6, "20-30", "MEDIA", "BASICO", "HOMEOPATICO", "ACTIVO"), "AMARILLO"); //8.4
		assertEquals(classHU1.getLevelToxicidad(6, "10-20", "ALTA", "ALCALINO", "MTP", "ACTIVO"), "AMARILLO"); //13.42
		assertEquals(classHU1.getLevelToxicidad(6, "10-20", "BAJA", "BASICO", "MTP", "EXCIPIENTE"), "VERDE"); //3.42
		assertEquals(classHU1.getLevelToxicidad(7, "10-20", "BAJA", "BASICO", "MTP", "EXCIPIENTE"), "AZUL"); //3.88
		assertEquals(classHU1.getLevelToxicidad(7, "10-20", "MEDIA", "ALCALINO", "MTP", "ACTIVO"), "AMARILLO"); //10.71
		assertEquals(classHU1.getLevelToxicidad(7, "20-30", "ALTA", "BASICO", "HOMEOPATICO", "ACTIVO"), "AMARILLO"); //10.6
	}
}
